var app = require('http').createServer(handleRequest);
var gpio = require('rpi-gpio');
var io = require('socket.io-client');
var socket = io.connect('http://138.197.18.2', { reconnect: true });

app.listen(3000);

const pin = 18;
var status = false;

gpio.setup(pin, gpio.DIR_OUT, writeOff);

function writeOn() {
    status = true;
    gpio.write(pin, true, function (err) {
        if (err) throw err;
        console.log('Written to pin' + pin);
    });
}
function writeOff() {
    status = false;
    gpio.write(pin, false, function (err) {
        if (err) throw err;
        console.log('Written to pin' + pin);
    });
}

function handleRequest(request, response) {
    response.end('It Works!! Path Hit: ' + request.url);
}

socket.on('connect', function () {
    console.log('Connected!');
});
socket.on('toggle', function () {
    console.log('toggled');
    console.log('Current status: ' + status);
    if(status) {
        console.log('Toggle off!');
        writeOff();
    } else {
        console.log('Toggle on!')
        writeOn();
    }
    console.log('Current status: ' + status);
});